# Benchmarks

Micro benchmark for `moka::future::Cache::get_with` method.

- https://github.com/moka-rs/moka/issues/123
- https://github.com/moka-rs/moka/pull/124

## Usage

```console
## Benchmark Moka v0.8.3
$ cargo bench --features moka083

## Benchmark Moka v0.8.4 (on ch123-get-with-if branch)
$ cargo bench --features moka084
```

## License

The MIT License.
