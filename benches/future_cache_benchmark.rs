use criterion::{black_box, criterion_group, criterion_main, Criterion};

#[cfg(feature = "moka083")]
use moka_083 as moka;

#[cfg(feature = "moka084")]
use moka_084 as moka;

use moka::future::Cache;

pub fn get_with(c: &mut Criterion) {
    let rt = tokio::runtime::Runtime::new().unwrap();

    const CAPACITY: u64 = 1000;
    let cache = Cache::builder()
        .max_capacity(CAPACITY)
        .initial_capacity(CAPACITY as usize)
        .build();
    let key1 = "key1".to_string();
    let key2 = "key2".to_string();

    c.bench_function("get_with", move |b| {
        b.to_async(&rt).iter(|| async {
            cache.invalidate(&key1).await;
            cache.invalidate(&key2).await;
            assert_eq!(
                cache
                    .get_with(key1.clone(), async { black_box("value1a".to_string()) })
                    .await,
                "value1a"
            );
            assert_eq!(
                cache
                    .get_with(key2.clone(), async { black_box("value2a".to_string()) })
                    .await,
                "value2a"
            );
            assert_eq!(
                cache
                    .get_with(key2.clone(), async { black_box("value2b".to_string()) })
                    .await,
                "value2a"
            );
            assert_eq!(
                cache
                    .get_with(key1.clone(), async { black_box("value1b".to_string()) })
                    .await,
                "value1a"
            );
            assert_eq!(
                cache
                    .get_with(key1.clone(), async { black_box("value1c".to_string()) })
                    .await,
                "value1a"
            );
            assert_eq!(
                cache
                    .get_with(key2.clone(), async { black_box("value2c".to_string()) })
                    .await,
                "value2a"
            );
        })
    });
}

criterion_group!(benches, get_with);
criterion_main!(benches);
